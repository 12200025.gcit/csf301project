import { storage, db, auth } from "../firebaseConfig";
import { deleteObject, ref } from 'firebase/storage';
import { getDownloadURL, uploadBytes } from "firebase/storage";
import { collection, addDoc, doc, updateDoc,getDocs, onSnapshot, query, getDoc, serverTimestamp, setDoc, where, increment, deleteDoc } from "firebase/firestore";
import { updateEmail, sendPasswordResetEmail, deleteUser } from "firebase/auth";

export const uploadImage = async (image, path,id) => {

  if (!image) {
    return;
  }  
  console.log("server  ",image);
  const response = await fetch(image)
  const blob = await response.blob()
  const filename = image.substring(image.lastIndexOf('/')+1)
  let storageRef;
  if (path === 'appProfile') {
    storageRef = ref(storage, `${path}/${id}`)
  } else {
    storageRef = ref(storage, `${path}/${id}/${filename}`)
  }
  try {
    await uploadBytes(storageRef,blob);
    const filePath = storageRef.fullPath;
    const imageUrl = await getDownloadURL(storageRef);
    console.log("file path ", filePath);
    return {filePath:filePath,imageUrl:imageUrl};
  } catch (error) {
    console.log(error);
    return;
  }
}

export const storeApp = async (name,image,category,appLink,snapshots,uid) => {
    try {
        console.log("name of the app", name)
        console.log("type of user id ", uid)
        const appRef = await addDoc(collection(db,"app"),{
            name:name,
            category:category,
            filePath:'',
            appLink:appLink,
            snapshots:[],
            userId:uid,
            snapshotsImageUrl:'https://firebasestorage.googleapis.com/v0/b/appcrate-7e3cf.appspot.com/o/appSnapshots%2FXuGOf6AlagmSqkqeVrCM%2F7564e337-5f65-4915-8371-cb005f82454b.jpeg?alt=media&token=eeb01936-d2ee-4820-a709-b5158ba15fdb',
            imageUrl:'https://firebasestorage.googleapis.com/v0/b/appcrate-7e3cf.appspot.com/o/appSnapshots%2FXuGOf6AlagmSqkqeVrCM%2F7564e337-5f65-4915-8371-cb005f82454b.jpeg?alt=media&token=eeb01936-d2ee-4820-a709-b5158ba15fdb',
            description:'',
            features:'',
            whatsNew:'',
            download:0
        });
        console.log("doc id is ",appRef.id)

        const {filePath,imageUrl} = await uploadImage(image,"appProfile",appRef.id);
        console.log("file path of image ", filePath)
        console.log("url of image ", imageUrl);
        const docRef = doc(db,"app",appRef.id);
        await updateDoc(docRef,{
            filePath:filePath,
            imageUrl:imageUrl
        })

        const snapshotsUrl = [];
        const snapshotsImageUrl = [];

        for (const snapshot of snapshots) {
            const {filePath,imageUrl} = await uploadImage(snapshot.uri,"appSnapshots",appRef.id);
            snapshotsUrl.push(filePath);
            snapshotsImageUrl.push(imageUrl);
        }
        await updateDoc(docRef,{
            snapshots:snapshotsUrl,
            snapshotsImageUrl:snapshotsImageUrl
        })
        return "Successfully Added!";
    } catch (error) {
        console.log(error.message);
        return "Failed to Add!";
    }
    
}
  
export const getApp = async () => {
  let allApp = [];
  const querySnapshot = await getDocs(collection(db,"app"));
  querySnapshot.forEach((doc) => {
    console.log(doc.id,"=>",doc.data());
    allApp.push({...doc.data(),doc_id:doc.id})
  })

  return allApp;
}

export const getAppWithCategory = async (callback) => {
  try {
    // const q = await query(collection(db,"app"), where("category","==",category))
    const colRef = collection(db,"app")
    // let allApp = [];
    // onSnapshot(colRef,(snapshot) => {
    //   let app = [];
    //   snapshot.docs.forEach((doc) => {
    //     app.push({...doc.data(),doc_id:doc.id})
    //   })
    //   allApp.push(...app);
    //   console.log("getting snapshot ", allApp)
    // })
    // return allApp;

    // await new Promise((resolve,reject) => {
    //   onSnapshot(colRef,docsSnap => {
    //     docsSnap.forEach(doc => {
    //       console.log("from server 1 ", doc.id,"=>",doc.data())
    //       allApp.push({...doc.data(),doc_id:doc.id})
    //     })
    //     console.log("getting snapshot ", allApp)
    //     resolve();
    //   },reject)
    // })
    // const querySnapshot = await getDocs(q);
    // querySnapshot.forEach((doc) => {
    //   console.log("from server ", doc.id,"=>",doc.data())
    //   allApp.push({...doc.data(),doc_id:doc.id})
    // })
    // return allApp;


    const unsubscribe = onSnapshot(colRef,(querySnapshot) => {
      const data = [];
      querySnapshot.forEach((doc) => {
        const docData = doc.data();
        data.push({...docData,doc_id:doc.id})
      });
      callback(data);
    })
    return unsubscribe;
  } catch (error) {
    console.log(error.message);
  }
}

export const getAllUser = async (callback) => {
  try {
    const colRef = collection(db,'users');
    const unsubscribe = onSnapshot(colRef,(querySnapshot) => {
      const data = [];
      querySnapshot.forEach((doc) => {
        const docData = doc.data();
        data.push({...docData, doc_id:doc.id})
      });
      callback(data);
    })
    return unsubscribe;
  } catch (error) {
    console.log(error.message);
  }
}

export const getSingleUserApp = async (user_id,callback) => {
  console.log("getting app")
  try {

    const q = await query(collection(db,"app"),where("userId","==",user_id))

    const unsubscribe = onSnapshot(q,(querySnapshot) => {
      const data = [];
      querySnapshot.forEach((doc) => {
        const docData = doc.data();
        data.push({...docData,doc_id:doc.id})
      });
      callback(data);
    })
    return unsubscribe;

    // const querySnapshot = await getDocs(q);
    // querySnapshot.forEach((doc) => {
    //   console.log("from server",doc.id,"=>",doc.data())
    //   allApp.push({...doc.data(),doc_id:doc.id})
    // })
    // return allApp;
  } catch (error) {
    console.log(error.message);
  }
}

export const giveComment = async (user_id,app_id,comment) => {
  try {
    const docRef = doc(db,"app",app_id);
    const colRef = collection(docRef,"comments")
    addDoc(colRef,{
      user_id: user_id, 
      comment:comment,
      createdAt:serverTimestamp()
    })
    return true;
  } catch (error) {
    console.log(error.message)
    return false
  }
}

export const getUser = async (user_id) => {
  if (!user_id) {
    return;
  }
  try {
    const docRef = doc(db,"users",user_id);
    const docSnap = await getDoc(docRef);

    if (docSnap.exists()) {
      return docSnap.data();
    } else {
      return {};
    }
  } catch (error) {
    console.log(error.message)
  }
}

async function getCommentUser(user_id) {
  if (!user_id) {
    return;
  }
  try {
    const docRef = doc(db,"users",user_id);
    const docSnap = await getDoc(docRef);

    if (docSnap.exists()) {
      return docSnap.data();
    } else {
      console.log("No such document")
    }
  } catch (error) {
    console.log("from ",error.message)
  }
}

export const getAllComment = async (app_id) => {
  try {
    const appRef = await doc(collection(db,'app'),app_id)
    const commentRef = await collection(appRef,'comments');

    const q = await query(commentRef);
    const allComment = [];

    const querySnapshot = await getDocs(q);

    for (const doc of querySnapshot.docs) {
      const user = await getCommentUser(doc.data().user_id);
      allComment.push({...doc.data(),doc_id:doc.id,email:user.email,username:user.username})
    }
    return allComment;
  } catch (error) {
    console.log(error.message)
  }
}
  

export const updateEmailUser = async (email, userName) => {
  console.log("changing email")
  try {
    const user_id = auth.currentUser.uid;
    await updateEmail(auth.currentUser,email)
    .then(async () => {
      await setDoc(doc(db,"users",user_id),{
        email:email,
        username:userName
      });
      return "Successfully Updated!"
    }).catch((error) => {
      console.log(error.message);
      return error.message;
    })
  } catch (error) {
    console.log(error.message);
    return error.message;
  }
}

export const verifyEmail = async (email) => {
  try {
    await sendPasswordResetEmail(auth,email)
    .then(() => {
      console.log("Sent")
    })
  } catch (error) {
    console.log(error.message);
  }
}

export const updateDescriptionOfApp = async (app_id,description,features,whatsNew,name) => {
  console.log(description,"in server")
  try {
    const docRef = doc(db,"app",app_id);
    await setDoc(docRef,{
      name:name,
      description:description,
      features:features,
      whatsNew:whatsNew
    },{merge:true})
    return true;
  } catch (error) {
    console.log(error.message);
    return false; 
  }
}

export const deleteAuthenticatedUser = async (uid) => {
  try {

    console.log("user id from server ",uid)
  } catch (error) {
    console.log(error.message);
  }
}

export const updateDownload = async (id) => {
  try {
    const docRef = doc(db,"app",id);
    await updateDoc(docRef,{
      download: increment(1)
    })
  } catch (error) {
    console.log(error.message);
  }
}

export const deleteApp = async (id,snapshots,filePath) => {
  try {
    console.log("Deleting app", id)
    console.log(snapshots)
    console.log(filePath)

    await deleteDoc(doc(db,"app",id))
    .then(() => {
      const iconRef = ref(storage,filePath);
      deleteObject(iconRef).then(() => {
        console.log("done");
      }).catch((error) => {
        console.log(error.message);
      })

      for (let i = 0; i < snapshots.length; i++) {
        console.log(snapshots[i])
        const snapRef = ref(storage,snapshots[i]);
        deleteObject(snapRef).then(() =>{
          
        }).catch((error) => {
          console.log(error.message);
        })
      }
    })
  } catch (error) {
    console.log("error from delete")
    console.log(error.message);
  }
}