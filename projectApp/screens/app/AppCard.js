import { View, TouchableOpacity, Image, Text } from "react-native";
import { Dimensions } from "react-native";
import { useNavigation } from "@react-navigation/native";

const windowWidth = Dimensions.get('window').width - 50;

export default function AppCard(props) {
    const navigation = useNavigation()

    return(
        <TouchableOpacity
            onPress={() => {
                navigation.navigate("Preview",{
                    image:props.image,
                    name:props.name,
                    type:props.type
                })
            }}
        >
            <View 
                style = {{
                    backgroundColor: "#D7E5DE",
                    width:windowWidth/3,
                    borderRadius:20,
                    justifyContent:'center',
                    alignItems:'center',
                    padding:5,
                    margin:5,
                    paddingVertical:20
                }}
            >
                <View style = {{width:50,height:50, borderWidth:1,borderRadius:50}}>
                    <Image
                        source={{uri:`${props.image}`}}
                        style = {{flex:1,resizeMode:'cover'}}
                    />
                </View>
                <View style = {{marginTop:5}}>
                    <Text style = {{fontSize:16, textAlign:'center'}}>{props.name}</Text>
                    <Text style = {{fontSize:12, textAlign:'center'}}>{props.type}</Text>
                </View>
            </View>
        </TouchableOpacity>
    )
}