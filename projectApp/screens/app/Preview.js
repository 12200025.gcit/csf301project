import { View, Image, Text, TouchableOpacity, Modal, TextInput, Dimensions, StyleSheet, Linking } from "react-native";
import { LinearGradient } from 'expo-linear-gradient';
import { SafeAreaView, ToastAndroid, ActivityIndicator } from "react-native";
import PreviewHeader from "./tabs/PreviewHeader";
import { Octicons, AntDesign } from "@expo/vector-icons"
import { ScrollView } from "react-native-gesture-handler";
import { useEffect, useState } from "react";
import { giveComment,getAllComment, updateDownload, getUser } from "../Server";
import AsyncStorage from "@react-native-async-storage/async-storage";

const windowWidth = Dimensions.get('window').width;

export default function Preview({navigation,route}) {

    const [modalVisible,setModalVisible] = useState(false);
    const [selectedImage,setSelectedImage] = useState('');

    const showToast = () => {
        ToastAndroid.showWithGravityAndOffset(
            'Thank You!',
            ToastAndroid.LONG,
            ToastAndroid.BOTTOM,
            25,
            50,
          );
    };

    const errorToast = () => {
        ToastAndroid.showWithGravityAndOffset(
            'Something went wrong. Try again!',
            ToastAndroid.LONG,
            ToastAndroid.BOTTOM,
            25,
            50,
          );
    };

    const noComment = () => {
        ToastAndroid.showWithGravityAndOffset(
            'Give some interesting comment.',
            ToastAndroid.LONG,
            ToastAndroid.BOTTOM,
            25,
            50,
          );
    };


    const {image,name,snapshots,user_id,app_id,category, description, features, whatsNew, appLink} = route.params;


    const [comment, setComment] = useState();
    const [sendingComment,setSendingComment] = useState(false);
    const [allComment,setAllComment] = useState([]);
    const [isDownloading,setIsDownloading] = useState(false);
    const [showComment,setShowComment] = useState(false);
    const [commentDuplicate,setCommentDuplicate] = useState();
    const [userId,setUserId] = useState();
    const [currentUser,setCurrentUser] = useState();

    const handleComment = async () => {
        setSendingComment(true)
        if (!comment) {
            noComment();
            setSendingComment(false)
            return;
        }
        try {
            setShowComment(true)
            const response = await giveComment(user_id,app_id,comment);
            setSendingComment(false)
            if (response) {
                showToast();
            } else {
                errorToast();
            }
        } catch (error) {
            errorToast();
            setSendingComment(false)
        } finally {
            setCommentDuplicate(comment);
            setComment("");
        }
    }

    const getComment = async () => {
        try {
            setAllComment(await getAllComment(app_id))

        } catch (error) {
            console.error(error.message) 
        }
    }

    const installApp = async () => {
        setIsDownloading(true)
        const apkUrl = appLink;
        try {
            const supported = await Linking.canOpenURL(apkUrl);
            if (supported) {
                await Linking.openURL(apkUrl);
                await updateDownload(app_id);
                setIsDownloading(false);
            } else {
                console.log("Cannot open url ", apkUrl);
            }
        } catch (err) {
            console.error(err.message);
        }
    }

    useEffect(() => {
        getComment();
    },[app_id])

    useEffect(() => {
        const gettingUserId = async () => {
            const value = await AsyncStorage.getItem("token")
            if (value !== null) {
                setUserId(value);
            }
        }
        gettingUserId();
    },[])

    useEffect(() => {
        const gettingUser = async () => {
            const user = await getUser(userId)
            setCurrentUser(user);
        }
        gettingUser();
    },[userId,setUserId])

    const openModal = (image) => {
        setSelectedImage(image);
        setModalVisible(true);
    }

    const closeModal = () => {
        setModalVisible(false);
    }

    return(
        <LinearGradient
            colors = {['rgba(58,131,244,0.4)','rgba(9,181,211,0.4)']}
            className = 'w-full flex-1'
        >

            <SafeAreaView style = {{marginTop:30}}>
                <View className = "container">
                <PreviewHeader/>
                    <ScrollView>
                    <View className = "" style = {{borderWidth:0.3, marginTop:5}}></View>
                    <View style = {{
                        marginTop:20
                    }}>
                        <View
                            style = {{flexDirection:'row', justifyContent:'space-between', alignItems:'center'}}
                        >
                            <View style = {{
                                flexDirection:'row',
                                justifyContent:'center',
                                alignItems:'center',
                                marginLeft:10
                            }}>
                                <Image
                                    className = "rounded-2xl"
                                    source={{uri: image}}
                                    style = {{width:100,height:100}}
                                />
                                <View
                                    style = {{
                                        marginLeft:10
                                    }}
                                >
                                    <Text className = "font-bold">{name}</Text>
                                    <Text>{category}</Text>
                                </View>
                            </View>
                        </View>
                        
                        <View className = "flex justify-center items-center mt-5"> 
                            <LinearGradient
                                colors = {['rgba(9,181,211,0.9)','rgba(58,131,244,0.9)']}
                                end = {{x:1, y: 0.2}}
                                start={{x: 0.1, y:0.2}}
                                className = {`rounded-full flex justify-center items-center`}
                                style = {{width:'90%'}}
                            >
                                <TouchableOpacity
                                    className = {`p-3 px-4`}
                                    onPress = {installApp}
                                >
                                    <Text className = "text-white font-bold">
                                        {isDownloading ? "Downloading...":"Download"}
                                    </Text>
                                </TouchableOpacity>

                            </LinearGradient>
                        </View>

                        <View style = {{marginTop:20, borderRadius:10}}>
                            <View
                                style = {{marginHorizontal:10,marginTop:10}}
                            >
                                 <ScrollView horizontal showsHorizontalScrollIndicator = {false}>
                                    {snapshots.length !== 0 &&
                                        snapshots.map((item,index) => (
                                            <View className = "mr-4" key = {index}>
                                                <TouchableOpacity
                                                    onPress={() => openModal(item)}
                                                >
                                                    <Image source={{uri:item}} style = {{width:150,height:300}}/>
                                                </TouchableOpacity>

                                                <Modal 
                                                    visible = {modalVisible} transparent = {true}
                                                    onRequestClose={closeModal}
                                                >
                                                    <View style = {styles.modalContainer}>
                                                        <Image
                                                            source={{uri:selectedImage}} style = {styles.fullImage} resizeMode="contain"
                                                        />
                                                        <TouchableOpacity style = {styles.closeButton}
                                                            onPress={closeModal}
                                                        >
                                                            <Text style = {styles.closeButtonText}>Close</Text>
                                                        </TouchableOpacity>
                                                    </View>
                                                </Modal>
                                            </View>
                                        ))
                                    }
                                </ScrollView>
                            </View>
                            <View className = "flex-row justify-between items-center mt-5">
                                <View className = "ml-4">
                                    <Text className = "font-bold">About this app</Text>
                                    <Text style = {{fontSize:10}}>Inspiring talks by remarkable people, accessible to anyone</Text>
                                </View>
                                <View className = "mr-4">
                                    <Octicons
                                        name="arrow-right"
                                        onPress={() => navigation.navigate("about",{
                                            description:description,
                                            features:features,
                                            whatsNew:whatsNew
                                        })}
                                        size={32}
                                    />
                                </View>
                            </View>

                            <View className = "mt-5" style = {{flexDirection:'row', justifyContent:'space-between', alignItems:'center', marginHorizontal:15}}>
                                <TextInput placeholder="Write a comment..."
                                    style = {{
                                        width:windowWidth/1.7,
                                        borderRadius:15,
                                        backgroundColor:'white',
                                        height:40,
                                        paddingLeft:20
                                    }}
                                    value = {comment}
                                    onChangeText = {(event) => setComment(event)}
                                />
                                {/* <TouchableOpacity style = {{...styles.button,backgroundColor:'white'}}
                                    onPress = {handleComment}
                                >
                                    <Text style = {styles.buttonText}>{sendingComment?<ActivityIndicator/>:"Send"}</Text>
                                </TouchableOpacity> */}
                                <LinearGradient
                                    colors = {['rgba(9,181,211,0.9)','rgba(58,131,244,0.9)']}
                                    end = {{x:1, y: 0.2}}
                                    start={{x: 0.1, y:0.2}}
                                    className = {`rounded-full`}
                                >
                                    <TouchableOpacity
                                        className = {`p-3 px-7`}
                                        onPress = {handleComment}
                                    >
                                        <Text className = "text-white font-bold">
                                            {sendingComment?<ActivityIndicator/>:"Send"}
                                        </Text>
                                    </TouchableOpacity>

                                </LinearGradient>
                            </View>

                            {/* data security */}
                            <View className = "container flex justify-center items-center mt-5">
                                <View className = "p-3" style = {{width:'70%', borderWidth:1, borderRadius:5}}>
                                    <View className = "flex-row">
                                        <AntDesign 
                                            name="sharealt" 
                                            size={15}
                                        />
                                        <Text className = "font-bold ml-2">
                                            No data shared with third parties.
                                        </Text>
                                    </View>
                                    <View className = "flex-row mt-2">
                                        <AntDesign
                                            name="cloudupload"
                                            size={15}
                                        />
                                        <View className = "ml-2">
                                            <Text className = "font-bold">
                                                This app my collect these data types
                                            </Text>
                                            <Text style = {{fontSize:10}}>
                                                Personal info, Photos and videos
                                            </Text>
                                        </View>
                                    </View>
                                    <View className = "mt-2 flex-row">
                                        <AntDesign
                                            name="unlock"
                                            size={15}
                                        />
                                        <Text className = "font-bold ml-2">
                                            Data isn't encrypted. Be aware of it.
                                        </Text>
                                    </View>
                                    <View className = "mt-2 flex-row">
                                        <AntDesign
                                            name="delete"
                                            size={15}
                                        />
                                        <Text className = "font-bold ml-2">
                                            You can request that data be deleted
                                        </Text>
                                    </View>
                                </View>
                            </View>
                            
                            <View style = {{marginTop:10, marginHorizontal:15}}>
                                <Text 
                                    style = {{
                                        fontSize:20
                                    }}
                                    className = "font-bold"
                                >Reviews</Text>
                                <View style = {{marginTop:10,marginBottom:80}}>
                                    {showComment &&
                                        <View style = {{borderRadius:10, marginBottom:10}}>
                                        <View className = "flex-row items-center">
                                            <View className = "flex justify-center items-center" style = {{width:40,height:40, borderRadius:50, marginLeft:10,backgroundColor:"#FE4C00"}}>
                                                <Text className = "font-bold text-white" style = {{fontSize:20}}>{currentUser.username.substring(0,1)}</Text>
                                            </View>
                                            <View style = {{marginLeft:10}}>
                                                <Text className = "font-bold">{currentUser.username}</Text>
                                            </View>
                                        </View>
                                        <View style = {{marginLeft:60}}>
                                            <Text style = {{fontSize:10}}>{new Date().toUTCString()}</Text>
                                        </View>
                                        <View className = "mt-2" style = {{marginLeft:60}}>
                                            <Text>{commentDuplicate}</Text>
                                            </View>
                                        </View>
                                    }
                                    {allComment && allComment.length !==0 && 
                                        allComment.map((item,index) => (
                                            <View style = {{borderRadius:10, marginBottom:10}} key = {index}>
                                                <View className = "flex-row items-center">
                                                    <View className = "flex justify-center items-center" style = {{width:40,height:40, borderRadius:50, marginLeft:10,backgroundColor:"#FE4C00"}}>
                                                        <Text className = "font-bold text-white" style = {{fontSize:20}}>{item.username.substring(0,1)}</Text>
                                                    </View>
                                                    <View style = {{marginLeft:10}}>
                                                        <Text className = "font-bold">{item.username}</Text>
                                                    </View>
                                                </View>
                                                <View style = {{marginLeft:60}}>
                                                    <Text style = {{fontSize:10}}>{new Date(item.createdAt.toDate()).toUTCString()}</Text>
                                                </View>
                                                <View className = "mt-2" style = {{marginLeft:60}}>
                                                    <Text>{item.comment}</Text>
                                                </View>
                                            </View>
                                        ))
                                    }
                                </View>
                            </View>
                        </View>
                    </View>
                    </ScrollView>
                </View>      
            </SafeAreaView>
        </LinearGradient>
    )
}

const styles = StyleSheet.create({
    button: {
        borderRadius:20,
    },
    buttonText: {
        paddingHorizontal:30,
        paddingVertical:10
    },
    modalContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
      },
      fullImage: {
        width: '80%',
        height: '80%',
      },
      closeButton: {
        position: 'absolute',
        top: 20,
        right: 20,
        padding: 10,
        backgroundColor: '#fff',
        borderRadius: 5,
      },
      closeButtonText: {
        fontSize: 16,
        fontWeight: 'bold',
      },
})