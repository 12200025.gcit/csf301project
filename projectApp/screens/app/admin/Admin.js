import { View, Text } from "react-native";
import { LinearGradient } from "expo-linear-gradient";
import AdminHeader from "./AdminHeader";


export default function Admin() {
    return (
        <LinearGradient
            colors = {['rgba(58,131,244,0.4)','rgba(9,181,211,0.4)']}
            className = 'w-full flex-1'
        >
            <View className = "mt-8">
                <AdminHeader/>
                <Text>In admin</Text>
            </View>
        </LinearGradient>

    )
}