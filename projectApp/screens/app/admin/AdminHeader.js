import { View,Image,Text,Dimensions, TextInput, TouchableOpacity } from "react-native"
import { useNavigation } from "@react-navigation/native";
import { auth } from "../../../firebaseConfig";
import { signOut } from "firebase/auth";
import {Entypo} from '@expo/vector-icons'

const windowWidth = Dimensions.get('window').width;

export default function AdminHeader() {

    const navigation = useNavigation();

    async function userSignOut() {
        signOut(auth)
        .then(() => {
            navigation.navigate("Login")
        })
        .catch((error) => {
            console.error(error);
        })
    }

    return(
        <View style = {{flexDirection:'row', justifyContent:'space-between', alignItems:'center', width:windowWidth}}>
            <TouchableOpacity style = {{marginLeft:10}}

            >
                <Image
                    source={{uri:'https://firebasestorage.googleapis.com/v0/b/appcrate-7e3cf.appspot.com/o/applogo.png?alt=media&token=046e47cd-0ef8-4c54-a8c8-f6017684ca0d'}}
                    style = {{width:40,height:40}}
                />
            </TouchableOpacity>
            <TouchableOpacity
                style = {{borderRadius:50,width:40,height:40, marginRight:10,justifyContent:'center'}}
                onPress={userSignOut}
            >
                <Entypo
                    name="log-out"
                    size={28}
                />
            </TouchableOpacity>
        </View>
    )
}