import { View, Text, ScrollView, TouchableOpacity,ActivityIndicator } from "react-native";
import { LinearGradient } from "expo-linear-gradient";
import AdminHeader from "./AdminHeader";
import { getAllUser } from "../../Server";
import { useEffect, useState } from "react";

export default function AdminUser() {

    const [allUser,setAllUser] = useState([]);
    const [isDeleting,setIsDeleting] = useState(false);

    const handleUserDelete = async (uid) => {
        setIsDeleting(true);
        try {
            const response = await fetch(`https://ae59-103-197-179-99.ngrok-free.app/appcrate-7e3cf/us-central1/app`);
            setIsDeleting(false);

        } catch (error) {
            setIsDeleting(false);
        }
    }

    useEffect(() => {
        let unsubscribe;

        const fetchData = async () => {
            unsubscribe = await getAllUser((updatedData) => {
                setAllUser(updatedData);
            })
        }

        fetchData();

        return () => {
            if (unsubscribe && typeof unsubscribe === 'function') {
                unsubscribe();
            }
        };
    },[])

    return (
        <LinearGradient
            colors = {['rgba(58,131,244,0.4)','rgba(9,181,211,0.4)']}
            className = 'w-full flex-1'
        >
            <View className = "mt-8">
                <AdminHeader/>
                <Text className = "font-bold mt-5 ml-4" style = {{fontSize:20}}>All User</Text>
                <ScrollView style = {{height:500, marginTop:20}} showsVerticalScrollIndicator = {false}>
                        {allUser.length !== 0 ? 
                        (
                            allUser.map((item,index) => {
                                return (
                                    <TouchableOpacity
                                        key={index}
                                        className = "mx-4 p-2 mb-2 flex-row border"
                                        style = {{borderRadius:10}}
                                    >
                                        <View className = "d-flex justify-center items-center" style = {{width:60,height:60, backgroundColor:"#FE4C00", borderRadius:50}}>
                                            <Text style = {{fontSize:30}}>{item.username.substring(0,1)}</Text>
                                        </View>
                                        <View className = "flex-1 flex justify-center pl-3 space-y-3">
                                            <Text className = "font-semibold">{item.username}</Text>
                                            <View>
                                                <Text style = {{fontSize:10}}>{item.email}</Text>
                                            </View>
                                        </View>
                                        <View className = "flex justify-center items-center">
                                            <LinearGradient
                                                colors = {['rgba(9,181,211,0.9)','rgba(58,131,244,0.9)']}
                                                end = {{x:1, y: 0.2}}
                                                start={{x: 0.1, y:0.2}}
                                                className = {`rounded-full items-center px-5`}
                                            >
                                                <TouchableOpacity
                                                    className = {`p-3 px-2`}
                                                    onPress={() => handleUserDelete(item.doc_id)}
                                                >
                                                    <Text className = "text-white font-bold">
                                                        {isDeleting?<ActivityIndicator/>:"Delete"}
                                                    </Text>
                                                </TouchableOpacity>

                                            </LinearGradient>
                                        </View>
                                    </TouchableOpacity>
                                )
                            })
                        ):(
                            <View className="" style = {{marginTop:'50%'}}>
                                <Text className="text-center" style={{ fontSize: 40 }}>
                                    🫥
                                </Text>
                                <Text className="text-center font-bold" style={{ fontSize: 15 }}>
                                    There isn't any user
                                </Text>
                            </View>
                        )
                            
                        }
                    </ScrollView>
            </View>
        </LinearGradient>
    )
}