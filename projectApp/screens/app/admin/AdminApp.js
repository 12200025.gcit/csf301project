import { View, Text, TouchableOpacity, Image, Alert } from "react-native";
import { LinearGradient } from "expo-linear-gradient";
import AdminHeader from "./AdminHeader";
import { useEffect, useState } from "react";
import { getAppWithCategory,deleteApp } from "../../Server";
import { ScrollView } from "react-native-gesture-handler";
import { AntDesign } from "@expo/vector-icons";

export default function AdminApp() {

    const [allApp,setAllApp] = useState([]);

    const deletingApp = async (app_id, snapshots,filePath) => {
        try {
            await deleteApp(app_id,snapshots,filePath);
        } catch (error) {
            console.error(error.message);
        }
    }

    const handleDelete = async (app_id,snapshots,filePath) => {
        try {

            Alert.alert(
                "Delete App",
                "Are you sure you want to delete?",
                [
                    {
                        text:'Cancel',
                        style:"cancel"
                    },
                    {
                        text:"Delete",
                        onPress: () => deletingApp(app_id,snapshots,filePath),
                        style: "destructive"
                    }
                ]
            )
                
        } catch (error) {
            console.error(error.message);
        }
    }

    useEffect(() => {
        let unsubscribe;

        const fetchData = async () => {
            unsubscribe = await getAppWithCategory((updatedData) => {
                setAllApp(updatedData);
            })
        }

        fetchData();

        return () => {
            if (unsubscribe && typeof unsubscribe === 'function') {
                unsubscribe();
            }
        };
    },[])

    return(
        <LinearGradient
            colors = {['rgba(58,131,244,0.4)','rgba(9,181,211,0.4)']}
            className = 'w-full flex-1'
        >
            <View className = "mt-8">
                <AdminHeader/>
                <Text className = "font-bold mt-5 ml-4" style = {{fontSize:20}}>All User App</Text>
                <ScrollView style = {{height:500, marginTop:20}} showsVerticalScrollIndicator = {false}>
                        {allApp.length !== 0 ?
                            (
                                allApp.map((item,index) => {
                                    return (
                                        <TouchableOpacity
                                            key={index}
                                            className = "mx-4 p-2 mb-2 flex-row border"
                                            style = {{borderRadius:10}}
                                        >
                                            <Image source = {{uri: item.imageUrl}} style = {{width:60,height:60}} className = "rounded-2xl"/>
                                            <View className = "flex-1 flex justify-center pl-3 space-y-3">
                                                <Text className = "font-semibold">{item.name}</Text>
                                                <View className = "flex-row items-center">
                                                    <AntDesign
                                                        name = "download"
                                                    />
                                                    <Text className = "text-gray-500 ml-1">0 k</Text>
                                                </View>
                                            </View>
                                            <View className = "flex justify-center items-center">
                                                <LinearGradient
                                                    colors = {['rgba(9,181,211,0.9)','rgba(58,131,244,0.9)']}
                                                    end = {{x:1, y: 0.2}}
                                                    start={{x: 0.1, y:0.2}}
                                                    className = {`rounded-full items-center px-5`}
                                                >
                                                    <TouchableOpacity
                                                        className = {`p-3 px-4`}
                                                        onPress={() => handleDelete(item.doc_id,item.snapshots,item.filePath)}
                                    
                                                    >
                                                        <Text className = "text-white font-bold">
                                                            Delete
                                                        </Text>
                                                    </TouchableOpacity>
    
                                                </LinearGradient>
                                            </View>
                                        </TouchableOpacity>
                                    )
                                })
                            ):(
                            <View className="" style = {{marginTop:'50%'}}>
                                <Text className="text-center" style={{ fontSize: 40 }}>
                                    🫥
                                </Text>
                                <Text className="text-center font-bold" style={{ fontSize: 15 }}>
                                    No Application
                                </Text>
                            </View>
                            )
                        }
                    </ScrollView>
            </View>
        </LinearGradient>
    )
}