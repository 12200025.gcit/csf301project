import { View, Text } from "react-native"
import { Octicons } from "@expo/vector-icons"
import { useNavigation } from "@react-navigation/native"

export default function PreviewHeader() {

    const navigation = useNavigation();

    return(
        <View style = {{flexDirection:'row', alignItems:'center'}}>
            <Octicons
                name="arrow-left"
                size={32}
                style = {{marginLeft:20}}
                onPress={() => navigation.navigate("TabHome")}
            />
            <Text style = {{fontWeight:'bold', marginLeft:30, fontSize:18}}>Preview</Text>
        </View>
    )
}