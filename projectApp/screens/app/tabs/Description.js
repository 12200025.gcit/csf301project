import { View, Text, TextInput, ActivityIndicator } from 'react-native'
import DescriptionHeader from './DescriptionHeader'
import { LinearGradient } from 'expo-linear-gradient'
import { Ionicons } from '@expo/vector-icons'
import { ScrollView } from 'react-native-gesture-handler'
import { useEffect, useState } from 'react'
import { updateDescriptionOfApp } from '../../Server'

export default function Description({route}) {
    const {appToEdit} = route.params;

    const [name,setName] = useState();
    const [description,setDescription] = useState();
    const [features,setFeatures] = useState();
    const [whatsNew,setWhatsNew] = useState();
    const [inputError,setInputError] = useState();
    const [descriptionError,setDescriptionError] = useState();
    const [featuresError,setFeaturesError] = useState();
    const [whatsNewError,setWhatsNewError] = useState();
    const [success,setSuccess] = useState();
    const [descriptionSuccess, setDescriptionSuccess] = useState();
    const [featuresSuccess,setFeaturesSuccess] = useState();
    const [whatsNewSuccess,setWhatsNewSuccess] = useState();

    //isloading indicator
    const [IsUpdatingName,setIsUpdatingName] = useState(false);
    const [isUpdatingDescription,setIsUpdatingDescription] = useState(false);
    const [isUpdatingFeatures,setIsUpdatingFeatures] = useState(false);
    const [isUpdatingWhatsNew,setIsUpdatingWhatsNew] = useState(false);
    

    const updateAppName = async () => {
        setIsUpdatingName(true);
        try {
            if (name === appToEdit.name) {
                setInputError("Already up-to-date.")
                setIsUpdatingName(false)
            } else {
                const response = await updateDescriptionOfApp(appToEdit.doc_id,description,features,whatsNew,name);
                if (response === true) {
                    setSuccess("Update Successfull!")
                    setInputError("");
                    setIsUpdatingName(false)
                } else {
                    setInputError("Something went wrong")
                    setIsUpdatingName(false)
                }
            }
        } catch (error) {
            console.error(error.message)
            setIsUpdatingName(false)
        }
    }

    const updateDescription = async () => {
        setIsUpdatingDescription(true)
        try {
            if (description === appToEdit.description) {
                setDescriptionError("Already up-to-date.")
                setIsUpdatingDescription(false)
            } else {
                const response = await updateDescriptionOfApp(appToEdit.doc_id,description,features,whatsNew,name);
                if (response === true) {
                    setDescriptionSuccess("Update Successfull!")
                    setDescriptionError("");
                    setIsUpdatingDescription(false)
                } else {
                    setDescriptionError("Something went wrong.")
                    setIsUpdatingDescription(false)
                }
            }
        } catch (error) {
            console.error(error.message)
            setIsUpdatingDescription(false)
        }
    }

    const updateFeatures = async () => {
        setIsUpdatingFeatures(true)
        try {
            if (features === appToEdit.features) {
                setFeaturesError("Already up-to-date.")
                setIsUpdatingFeatures(false)
            } else {
                const response = await updateDescriptionOfApp(appToEdit.doc_id,description,features,whatsNew,name);
                if (response === true) {
                    setFeaturesSuccess("Update Successfull!")
                    setFeaturesError("");
                    setIsUpdatingFeatures(false)
                } else {
                    setFeaturesError("Something went wrong.")
                    setIsUpdatingFeatures(false)
                }
            }
        } catch (error) {
            console.error(error.message)
            setIsUpdatingFeatures(false)
        }
    }

    const updateWhatsNew = async () => {
        setIsUpdatingWhatsNew(true)
        try {
            if (whatsNew === appToEdit.whatsNew) {
                setWhatsNewError("Already up-to-date.")
                setIsUpdatingWhatsNew(false)
            } else {
                const response = await updateDescriptionOfApp(appToEdit.doc_id,description,features,whatsNew,name);
                if (response === true) {
                    setWhatsNewSuccess("Update Successfull!");
                    setWhatsNewError("");
                    setIsUpdatingWhatsNew(false)
                } else {
                    setWhatsNewError("Something went wong.");
                    setIsUpdatingWhatsNew(false)
                }
            }
        } catch (error) {
            console.error(error.message)
            setIsUpdatingWhatsNew(false)
        }
    }

    const setAll = () => {
        setName(appToEdit.name)
        setDescription(appToEdit.description)
        setFeatures(appToEdit.features)
        setWhatsNew(appToEdit.whatsNew)
    }

    useEffect(() => {
        setAll();
        return;
    },[])
    
  return (
        <LinearGradient
            colors = {['rgba(58,131,244,0.4)','rgba(9,181,211,0.4)']}
            className = 'w-full flex-1'
        >   
            <DescriptionHeader/>
            <ScrollView>
            <View className = "mt-6 flex-row justify-center items-center">
                <View className = "" style = {{width:'80%', marginBottom:20}}>
                    <View>
                        <View>
                            <Text className = "font-bold mb-2">App Name</Text>
                            <View style = {{position:'relative'}}>
                                <TextInput value= {name} className = "border px-2" style = {{height:40, borderRadius:5, width:'87%'}}
                                    onChangeText = {(event) => setName(event)}
                                />
                                {IsUpdatingName?(
                                    <ActivityIndicator/>
                                ):(
                                    <Ionicons
                                        name='md-add-circle-sharp'
                                        size={30}
                                        color="#FE4C00"
                                        style = {{position:'absolute',top:'10%', right:0,}}
                                        onPress={updateAppName}
                                    />
                                )}
                            </View>
                            {inputError && 
                                <Text className = "ml-2" style = {{fontSize:12, color:'#FE4C00'}}>{inputError}</Text>
                            }
                            {success && 
                                <Text className = "ml-2" style = {{fontSize:12, color:'green'}}>{success}</Text>
                            }
                        </View>

                        <View className = "mt-5">
                            <Text className = "font-bold mb-2">About this APP</Text>
                            <View style = {{position:'relative'}}>
                                <TextInput value = {description} className = "border px-2" style = {{borderRadius:5, width:'87%', textAlignVertical:'top', paddingTop:5}}
                                    multiline={true}
                                    numberOfLines={10}
                                    onChangeText = {(event) => setDescription(event)}
                                />
                                {isUpdatingDescription ? (
                                    <ActivityIndicator/>
                                ):(
                                    <Ionicons
                                        name='md-add-circle-sharp'
                                        size={30}
                                        color="#FE4C00"
                                        style = {{position:'absolute',top:'70%', right:0,}}
                                        onPress={updateDescription}
                                    />
                                )
                                }
                            </View>
                            {descriptionError && 
                                <Text className = "ml-2" style = {{fontSize:12, color:'#FE4C00'}}>{descriptionError}</Text>
                            }
                            {descriptionSuccess && 
                                <Text className = "ml-2" style = {{fontSize:12, color:'green'}}>{success}</Text>
                            }
                        </View>

                        <View className = "mt-5">
                            <Text className = "font-bold mb-2">Features</Text>
                            <View style = {{position:'relative'}}>
                                <TextInput value = {features} className = "border px-2" style = {{borderRadius:5, width:'87%', textAlignVertical:'top', paddingTop:5}}
                                    multiline={true}
                                    numberOfLines={10}
                                    onChangeText = {(event) => setFeatures(event)}
                                />
                                {isUpdatingFeatures ? (
                                    <ActivityIndicator/>
                                ):(
                                    <Ionicons
                                        name='md-add-circle-sharp'
                                        size={30}
                                        color="#FE4C00"
                                        style = {{position:'absolute',top:'70%', right:0,}}
                                        onPress={updateFeatures}
                                    />
                                )

                                }
                            </View>
                            {featuresError && 
                                <Text className = "ml-2" style = {{fontSize:12, color:'#FE4C00'}}>{inputError}</Text>
                            }
                            {featuresSuccess && 
                                <Text className = "ml-2" style = {{fontSize:12, color:'green'}}>{success}</Text>
                            }
                        </View>

                        <View className = "mt-5">
                            <Text className = "font-bold mb-2">What's New?</Text>
                            <View style = {{position:'relative'}}>
                                <TextInput className = "border px-2" style = {{borderRadius:5, width:'87%', textAlignVertical:'top', paddingTop:5}}
                                    multiline={true}
                                    numberOfLines={10}
                                    value = {whatsNew}
                                    onChangeText = {(event) => setWhatsNew(event)}
                                />
                                {isUpdatingWhatsNew ? (
                                    <ActivityIndicator/>
                                ):(
                                    <Ionicons
                                        name='md-add-circle-sharp'
                                        size={30}
                                        color="#FE4C00"
                                        style = {{position:'absolute',top:'70%', right:0,}}
                                        onPress={updateWhatsNew}
                                    />
                                )

                                }
                            </View>
                            {whatsNewError && 
                                <Text className = "ml-2" style = {{fontSize:12, color:'#FE4C00'}}>{inputError}</Text>
                            }
                            {whatsNewSuccess && 
                                <Text className = "ml-2" style = {{fontSize:12, color:'green'}}>{success}</Text>
                            }
                        </View>
                    </View>
                </View>
            </View>
            </ScrollView>
        </LinearGradient>
  )
}
