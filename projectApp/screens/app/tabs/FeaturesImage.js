import { useEffect,useState } from 'react';
import { View, StyleSheet, Image, Dimensions } from 'react-native'
import Swiper from 'react-native-swiper';

const { height } = Dimensions.get('window');

export default function FeaturesImage() {

    const [featureImage,setFeatureImage] = useState([]);

    useEffect(() => {
        setFeatureImage(
            [
                'https://leverageedu.com/blog/wp-content/uploads/2020/08/Free-Educational-Apps-for-Students.jpg',
                'https://images.airdroid.com/2022/03/famisafe-android-tracking-app.jpg',
                'https://thesocialelement.agency/wp-content/uploads/2022/08/unnamed.jpg',
                'https://storage.googleapis.com/platform-qa-segments/micro-apps/rush-web/images/thumbnail1.png',
                'https://sparkful-assets.s3.ap-northeast-1.amazonaws.com/packages/apps/to-do-adventure-460.png'
            ]
        );
        return () => {
            setFeatureImage([]);
        }
    },[])

    return (
        <View style = {styles.rootContainer}>
            <Swiper
                showsButtons = {true}
                autoplay = {true}
                autoplayTimeout = {3}
                buttonWrapperStyle = {{
                    paddingHorizontal:0,
                    paddingVertical: 0,
                }}
            >
                {featureImage.map((image) => (
                    <View style = {styles.imageContainer} key = {image}>
                        <Image style = {styles.image} source = {{uri:image}}/>
                    </View>
                ))}
            </Swiper>
        </View>
    )
}

const styles = StyleSheet.create({
    rootContainer: {
      width: "100%",
      height: height / 4,
      borderRadius: 10,
      overflow: "hidden",
    },
    image: {
      width: "100%",
      height: "100%",
      borderRadius: 10,
      overflow: "hidden",
    },
    imageContainer: {
      paddingHorizontal: 15,
    },
  });
  