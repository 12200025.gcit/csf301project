import { TextInput, Dimensions, Linking } from "react-native";
import { View, Image, Text } from "react-native";
import { useEffect, useState } from "react";
import { getAppWithCategory, updateDownload } from "../../Server";
import { LinearGradient } from 'expo-linear-gradient';
import { SafeAreaView } from "react-native-safe-area-context";
import { ScrollView } from "react-native";
import { TouchableOpacity } from "react-native";
import GradientButton from "./GradientButton";
import { AntDesign } from "@expo/vector-icons";
import FeaturesImage from "./FeaturesImage";
import AsyncStorage from "@react-native-async-storage/async-storage";

const categories = ['Education','Entertainment','Social','Games','Business']

const windowWidth = Dimensions.get('window').width;

export default function Home({ navigation }) {

    const [userId,setUserId] = useState();
    const [allApp,setAllApp] = useState([]);
    const [activeCategory,setActiveCategory] = useState("Education");
    const [collapse,setCollapse] = useState(false);
    const [isSearching,setIsSearching] = useState(false);
    const [searchQuery,setSearchQuery] = useState('');
    const [searchedData,setSearchedData] = useState([]);

    useEffect(() => {
        const gettingUserId = async () => {
            const value = await AsyncStorage.getItem("token")
            if (value !== null) {
                setUserId(value);
            }
        }
        gettingUserId();
    },[])

    useEffect(() => {
        let unsubscribe;

        const fetchData = async () => {
            unsubscribe = await getAppWithCategory((updatedData) => {
                setAllApp(updatedData);
            })
        }

        fetchData();

        return () => {
            if (unsubscribe && typeof unsubscribe === 'function') {
                unsubscribe();
            }
        };
    },[])

    useEffect(() => {
        if (searchQuery === '') {
            setIsSearching(false)
        } else {
            setIsSearching(true)
        }
        const data = allApp.filter(item => item.name.toLowerCase().includes(searchQuery.toLowerCase()));
        setSearchedData(data);
    },[searchQuery])

    const installApp = async (appLink,app_id) => {
        const apkUrl = appLink;
        const supported = await Linking.canOpenURL(apkUrl);
        if (supported) {
            await Linking.openURL(apkUrl);
            await updateDownload(app_id);
        }
    }

    return(
        <LinearGradient
            colors = {['rgba(58,131,244,0.4)','rgba(9,181,211,0.4)']}
            className = 'w-full flex-1'
        >
            <SafeAreaView>
            <View className = "container">

            {/* header   */}
            <View style = {{flexDirection:'row', justifyContent:'space-between', alignItems:'center', width:windowWidth}}>
                <TouchableOpacity style = {{marginLeft:10}}
                    onPress = {() => navigation.openDrawer()}
                >
                    <Image
                        source={require("../../../assets/logo/applogo.png")}
                        style = {{width:40,height:40}}
                    />
                </TouchableOpacity>
                <TextInput
                    value = {searchQuery}
                    onChangeText = {(event) => setSearchQuery(event)}
                    placeholder="Search for apps...."
                    style = {{backgroundColor: "#D7E5DE", borderRadius:20, width:windowWidth/1.5, paddingLeft:20, height:35}}
                />
                <TouchableOpacity
                    style = {{borderRadius:50,width:40,height:40, marginRight:10,justifyContent:'center'}}
                    onPress={() => {
                        navigation.openDrawer();
                    }}
                >
                    <Image
                        source={{uri:'https://www.communardo.com/wp-content/uploads/2017/01/User-Profiles_701x701.png'}}
                        style = {{width:40, height:40, borderRadius:50}}
                    />
                </TouchableOpacity>
            </View>

                <View style = {{borderWidth:0.3, marginTop:10}}></View>
                {!isSearching ? (
                    <View className = "mt-3 space-y-3">
                    <Text className = "ml-4 font-bold" style = {{fontSize:20}}>
                        Browse Apps
                    </Text>
                    {/* category */}
                    <View className = "pl-4">
                        <ScrollView horizontal showsHorizontalScrollIndicator = {false}>
                            {
                                categories.map(cat => {
                                    if (cat === activeCategory) {
                                        return (
                                            <GradientButton key = {cat} containerClass = 'mr-2' value = {cat}/>
                                        )
                                    } else {
                                        return(
                                            <TouchableOpacity
                                                onPress = {() => setActiveCategory(cat)}
                                                key={cat}
                                                className = "bg-blue-200 p-3 px-4 rounded-full mr-2"
                                            >
                                                <Text>{cat}</Text>
                                            </TouchableOpacity>
                                        )
                                    }
                                })
                            }
                        </ScrollView>
                    </View>

                    {/* feature row */}
                    <View className = "mt-3 space-y-3 mb-4">
                        <View className = "flex-row justify-between items-center">
                            <View>
                                <Text className = "ml-4 font-bold">
                                    Features
                                </Text>
                            </View>
                            <TouchableOpacity
                                className = "mr-4"
                                onPress = {() => setCollapse(!collapse)}
                            >
                                <Text className = "font-bold">{collapse?"Show":"Collapse"}</Text>
                            </TouchableOpacity>
                        </View>
                        
                        <View className = {`pl-4 ${collapse?"hidden":null}`}>
                            <FeaturesImage/>
                        </View>
                    </View>

                    {/* education list */}
                    <View className = "flex-row justify-between items-center mb-2">
                        <Text className = "ml-4 font-bold">
                            Top {activeCategory} Apps
                        </Text>
                    </View>
                    <ScrollView style = {{height:500}} showsVerticalScrollIndicator = {false}>
                    {
                        allApp.filter((data) => data.category === activeCategory).length === 0 && 
                        <View className="mt-5">
                            <Text className="text-center" style={{ fontSize: 40 }}>
                                🫥
                            </Text>
                            <Text className="text-center font-bold" style={{ fontSize: 10 }}>
                                This category contains no app.
                            </Text>
                        </View>
                    }
                    {allApp.length !== 0 && (
                        allApp.map((item, index) => {
                        if (item.category === activeCategory) {
                            return (
                            <TouchableOpacity
                                key={index}
                                className="mx-4 p-2 mb-2 flex-row border"
                                style={{ borderRadius: 10 }}
                                onPress={() => {
                                navigation.navigate('Preview', {
                                    image: item.imageUrl,
                                    name: item.name,
                                    snapshots: item.snapshotsImageUrl,
                                    user_id: userId,
                                    app_id: item.doc_id,
                                    category: item.category,
                                    description: item.description,
                                    features: item.features,
                                    whatsNew: item.whatsNew,
                                    appLink: item.appLink,
                                });
                                }}
                            >
                                <Image
                                source={{ uri: item.imageUrl }}
                                style={{ width: 60, height: 60 }}
                                className="rounded-2xl"
                                />
                                <View className="flex-1 flex justify-center pl-3 space-y-3">
                                <Text className="font-semibold">{item.name}</Text>
                                <View className="flex-row items-center">
                                    <AntDesign name="download" />
                                    <Text className="text-gray-500 ml-1">{item.download}</Text>
                                </View>
                                </View>
                                <View className="flex justify-center">
                                    <LinearGradient
                                        colors = {['rgba(9,181,211,0.9)','rgba(58,131,244,0.9)']}
                                        end = {{x:1, y: 0.2}}
                                        start={{x: 0.1, y:0.2}}
                                        className = {`rounded-full items-center px-3`}
                                    >
                                        <TouchableOpacity
                                            className = {`p-2 px-3`}
                                            onPress = {() => installApp(item.appLink,item.doc_id)}
                                        >
                                            <Text className = "text-white font-bold">
                                                Get
                                            </Text>
                                        </TouchableOpacity>

                                    </LinearGradient>
                                </View>
                            </TouchableOpacity>
                            );
                        } else {
                            return null;
                        }
                        })
                    )}
                    </ScrollView>
                </View>
                ):(
                    <View className = "mt-4">
                    <ScrollView style = {{height:500}} showsVerticalScrollIndicator = {false}>
                    {searchedData.length !== 0 ? (
                        searchedData.map((item, index) => {
                            return (
                                <TouchableOpacity
                                key={index}
                                className="mx-4 p-2 mb-2 flex-row border"
                                style={{ borderRadius: 10 }}
                                onPress={() => {
                                navigation.navigate('Preview', {
                                    image: item.imageUrl,
                                    name: item.name,
                                    snapshots: item.snapshotsImageUrl,
                                    user_id: userId,
                                    app_id: item.doc_id,
                                    category: item.category,
                                    description: item.description,
                                    features: item.features,
                                    whatsNew: item.whatsNew,
                                });
                                }}
                            >
                                <Image
                                source={{ uri: item.imageUrl }}
                                style={{ width: 60, height: 60 }}
                                className="rounded-2xl"
                                />
                                <View className="flex-1 flex justify-center pl-3 space-y-3">
                                <Text className="font-semibold">{item.name}</Text>
                                <View className="flex-row items-center">
                                    <AntDesign name="download" />
                                    <Text className="text-gray-500 ml-1">{item.download}</Text>
                                </View>
                                </View>
                                <View className="flex justify-center">
                                <GradientButton value="Get" buttonClass="py-2 px-5" />
                                </View>
                            </TouchableOpacity>
                            )
                        
                        })
                    ) : (
                        <View className="mt-5">
                        <Text className="text-center" style={{ fontSize: 40 }}>
                            🫥
                        </Text>
                        <Text className="text-center font-bold" style={{ fontSize: 10 }}>
                            This category contains no app.
                        </Text>
                        </View>
                    )}
                    </ScrollView>
                    </View>
                )}
            </View>
            </SafeAreaView>
            
        </LinearGradient>
    )
}