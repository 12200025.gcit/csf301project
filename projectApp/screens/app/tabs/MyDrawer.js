import { View, Text } from "react-native";
import { createDrawerNavigator, DrawerContentScrollView, DrawerItemList, DrawerItem } from "@react-navigation/drawer"
import Home from "./Home";
import Upload from "../Upload";
import UserProfile from "../../user/UserProfile";
import { AntDesign, MaterialCommunityIcons } from "@expo/vector-icons";
import { useEffect, useState } from "react";
import {signOut } from "firebase/auth";
import { auth } from "../../../firebaseConfig";
import { getUser } from "../../Server";
import AsyncStorage from '@react-native-async-storage/async-storage';

const Drawer = createDrawerNavigator();

const CustomDrawerContent = (props) => {

    async function userSignOut() {
        signOut(auth)
        .then(async () => {
            await AsyncStorage.removeItem("token")
            props.navigation.replace("Login")
        })
        .catch((error) => {
            console.error(error);
        })
    }

    return (
        <DrawerContentScrollView>
            <View style = {{display:'flex', justifyContent:'center', alignItems:'center',marginBottom:30,marginTop:30}}>
                <View style = {{display:'flex', flexDirection:'column',justifyContent:'center',alignItems:'center'}}>
                    <View style = {{height:50,width:50,borderRadius:50,backgroundColor:"#FE4C00", flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
                        <Text style = {{fontWeight:'bold', fontSize:30,color:'white'}}>{props.name?props.name.substring(0,1):""}</Text>
                    </View>
                    <Text style = {{marginTop:6, fontWeight:'bold', fontSize:15}}>{props.name?props.name:""}</Text>
                    <Text style = {{marginTop:6, fontWeight:'bold', fontSize:15}}>{props.email?props.email:""}</Text>
                </View>
            </View>
            <DrawerItemList {...props}/>
            <DrawerItem 
                label="Sign Out" 
                onPress={userSignOut}
                icon={() => <AntDesign name="logout" size={24} color="black" />}
            />
        </DrawerContentScrollView>
    )
}

const MyDrawer = () => {

    const [userId,setUserId] = useState();
    const [user,setUser] = useState([]);

    const gettingUser = async () => {
        try {
            setUser(await getUser(userId))
        } catch (error) {
            console.error(error.message);
        }
    }

    useEffect(() => {
        const gettingToken = async () => {
            const value = await AsyncStorage.getItem("token")
            if (value !== null) {
                setUserId(value);
            }
        }

        gettingToken();
    },[])

    useEffect(() => {
        gettingUser();
        return;
    },[userId])

    return (
        <Drawer.Navigator
            screenOptions={{headerShown:false}}
            drawerContent={(props) => <CustomDrawerContent {...props} name = {user?user.username:null} email = {user?user.email:null}/>}
        >
            <Drawer.Screen name = "Home" component = {Home}
                options={{drawerIcon:(() => (
                    <AntDesign
                        name="home"
                        size={25}
                    />
                ))}}
            />
            <Drawer.Screen name = "Add" component={Upload}
                options={{drawerIcon:(() => (
                    <AntDesign
                        name="upload"
                        size={25}
                    />
                ))}}
            />
            <Drawer.Screen name = "Profile" component={UserProfile}
                options={{drawerIcon:(() => (
                    <MaterialCommunityIcons
                        name="face-man-profile"
                        size={25}
                    />
                ))}}
            />
        </Drawer.Navigator>
    )
}

export default MyDrawer;