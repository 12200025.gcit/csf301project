import { View,Image, Text } from "react-native";
import {LinearGradient} from 'expo-linear-gradient';

export default function FeatureCard({app}) {
    return(
        <View className = "mr-4 relative">
            <Image source={{uri:`${app.image}`}} className = "w-80 h-60 rounded-3xl"/>
                <LinearGradient
                    colors = {['rgba(9,170,200,0.2)','rgba(30,13,230,0.5)']}
                    end = {{x:1, y: 0.2}}
                    start={{x: 0.1, y:0.2}}
                    className = {`absolute left-5 top-5 rounded-l-lg`}
                >
                    <View className = "shadow-lg">
                        <Text className = "text-white p-2 font-bold">{app.name}</Text>
                    </View>
                </LinearGradient>
                
                <View className = "absolute inset-x-0 bottom-5 left-5 bg-gray-500 rounded-l-lg shadow-lg">
                    <Text className = "text-white p-2">{app.description}</Text>
                </View>
        </View>
    )
}