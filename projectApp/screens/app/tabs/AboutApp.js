import { View,Text, SafeAreaView } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';

export default function AboutApp({route}) {
  const {description, features, whatsNew} = route.params;
  return (
    <LinearGradient
        colors = {['rgba(58,131,244,0.4)','rgba(9,181,211,0.4)']}
        className = 'w-full flex-1'
    >
      <SafeAreaView className = "mt-8">
        <View className = "container p-5">
          <Text className = "font-bold" style = {{fontSize:20}}>About this app</Text>
          {description ? (
            <View style = {{borderWidth:1, borderRadius:5,height:100}}>
              <Text className = "px-2">{description}</Text>
            </View> ):(
              <View>
                <Text>Nothing</Text>
              </View>
            )
          }

          <Text className = "font-bold mt-5" style = {{fontSize:20}}>Features</Text>
          {description ? (
            <View style = {{borderWidth:1, borderRadius:5,height:100}}>
              <Text className = "px-2">{features}</Text>
            </View> ):(
              <View>
                <Text>Nothing</Text>
              </View>
            )
          }

          <Text className = "font-bold mt-5" style = {{fontSize:20}}>What's New</Text>
          {description ? (
            <View style = {{borderWidth:1, borderRadius:5,height:100}}>
              <Text className = "px-2">{whatsNew}</Text>
            </View> ):(
              <View>
                <Text>Nothing</Text>
              </View>
            )
          }
        </View>
      </SafeAreaView>
    </LinearGradient>
  )
}
