import { createBottomTabNavigator } from "@react-navigation/bottom-tabs"
import Home from "./Home";
import { MaterialIcons } from "@expo/vector-icons";
import Upload from "../Upload";
import AdminApp from "../admin/AdminApp";
import AdminUser from "../admin/AdminUser";

const Tab = createBottomTabNavigator();

export default function BottomTab() {
    return(
        <Tab.Navigator
            screenOptions={{
                headerShown: false,
            }}
        >
            <Tab.Screen
                name = "Apps"
                component={AdminApp}
                options={{
                    tabBarIcon: (tabinfo) => (
                        <MaterialIcons
                            name = "apps"
                            color={tabinfo.focused?"blue":"black"}
                            size = {24}
                        />
                    )
                }}
            />

            <Tab.Screen
                name = "Users"
                component={AdminUser}
                options={{
                    tabBarIcon: (tabinfo) => (
                        <MaterialIcons
                            name="supervised-user-circle"
                            color={tabinfo.focused?"blue":"black"}
                            size={24}
                        />
                    )
                }}
            />
        </Tab.Navigator>
    )
}