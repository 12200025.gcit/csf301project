import { View, Text, StyleSheet, Dimensions,TouchableOpacity, TextInput,Image, Alert, ActivityIndicator } from "react-native";
import Header from "./Header";
import { MaterialIcons } from "@expo/vector-icons";
import { useEffect, useState } from "react";
import * as ImagePicker from 'expo-image-picker';
import { SelectList } from "react-native-dropdown-select-list";
import { FlatList } from "react-native";
import { Modal } from "react-native";
import { storeApp } from "../Server";
import { LinearGradient } from "expo-linear-gradient";
import AsyncStorage from "@react-native-async-storage/async-storage";

const windowHeight = Dimensions.get('window').height;

export default function Upload() {

    const [image,setImage] = useState("");
    const [name,setName] = useState();
    const [category,setCategory] = useState("");
    const [appLink, setAppLink] = useState();
    const [snapshots,setSnapshots] = useState([]);
    const [modalVisible, setModalVisible] = useState(false);
    const [fieldError, setFieldError] = useState("");
    const [responseFromServer,setResponseFromServer] = useState("");
    const [isLoading,setIsLoading] = useState(false);
    const [userId,setUserId] = useState();

    useEffect(() => {
        const gettingToken = async () => {
            const userId = await AsyncStorage.getItem("token")
            if (userId !== null) {
                setUserId(userId)
            }
        }
        gettingToken();
    },[])


    const data = [
        {key: '1', value: 'Entertainment'},
        {key: '2', value: 'Education'},
        {key: '3', value: 'Social'},
        {key: '4', value: 'Game'},
        {key: '5', value: 'Business'}
    ]

    const handleChooseImage = async () => {
        let result = await ImagePicker.launchImageLibraryAsync({
            mediaTypes:ImagePicker.MediaTypeOptions.Images,
            allowsEditing: true,
            aspect:[4,3],
            quality:1
        });

        if (!result.canceled) {
            setImage(result.assets[0].uri);
        }
    }

    const pickImages = async () => {
        setSnapshots("");
        let result = await ImagePicker.launchImageLibraryAsync({
            mediaTypes:ImagePicker.MediaTypeOptions.Images,
            allowsMultipleSelection: true,
            aspect:[4,3],
            quality:1
        });

        if (!result.canceled) {
            setSnapshots(result.assets);
        }
    }

    const deleteImage = (id) => {
        setSnapshots((current) => 
            current.filter((snapshot) => snapshot.assetId !== id)
        );
    }

    const handleUpload = async () => {
        setIsLoading(true);
        if (!image || !name || category === '' || !appLink || snapshots.length === 0) {
            setFieldError("All your input must be filled.")
            setIsLoading(false)
        } else {
            setFieldError(""); 
            const response = await storeApp(name,image,category,appLink,snapshots,userId);
            setResponseFromServer(response);
            setIsLoading(false);
            setAppLink("");
            setCategory("");
            setName("");
            setSnapshots([]);
            setImage("");
        }
        
    }

    return(
        <LinearGradient
            colors = {['rgba(58,131,244,0.4)','rgba(9,181,211,0.4)']}
            className = 'w-full flex-1'
        >
             <View style = {{marginTop:30}}>
                <Header/>
                <View style = {{borderWidth:0.3, marginTop:15}}></View>
                <View style = {styles.container}>
                    <View style = {{marginTop:-50,marginBottom:10}}>
                        <Text style = {{fontWeight:'bold',fontSize:20}}>Upload Your APP</Text>
                    </View>
                    {fieldError ? <View style = {{marginBottom:10, borderRadius:10, backgroundColor:'#FA8072', width:300}}><Text style = {{padding:10,textAlign:'center', color:'white',paddingVertical:15,fontWeight:'bold', paddingHorizontal:40}}>{fieldError}</Text></View>:null}
                    <TouchableOpacity
                        onPress={handleChooseImage}
                        style = {{flexDirection:image?'row':null}}
                    >
                    {image ? (
                        <View style={{ justifyContent: 'center', alignItems: 'center', borderWidth: 2, borderRadius: 5, height: 80, width: 100, paddingHorizontal: 10, borderStyle: 'dashed', marginLeft: 10 }}>
                            <Image source={{ uri: image }} style={{ width: 100, height: 80 }} />
                        </View>
                        ) : (
                        <View style={{ justifyContent: 'center', alignItems: 'center', borderWidth: 2, borderRadius: 5, height: 80, width: 100, paddingHorizontal: 10, borderStyle: 'dashed', marginBottom: 10 }}>
                            <MaterialIcons name="file-upload" size={34} />
                            <Text style={{ color: 'blue', fontSize: 10, width: '100%', textAlign: 'center' }}>Upload Image</Text>
                        </View>
                    )}
                    </TouchableOpacity>
                    <View style = {{marginVertical:10}}>
                        <TextInput placeholder="App Name"
                            value={name}
                            onChangeText={(event) => setName(event)}
                            style = {{...styles.inputField,borderWidth:1,width:300,height:45}}
                        />

                        <TextInput placeholder="Add apk link"
                            value = {appLink}
                            onChangeText={(event) => setAppLink(event)}
                            style = {{...styles.inputField,borderWidth:1,width:300,height:45}}
                        />

                        <SelectList
                            setSelected={(val) => setCategory(val)}
                            data = {data}
                            save = "value"
                        />
                        
                        <TouchableOpacity
                            onPress = {pickImages}
                        >
                            <View style = {{justifyContent:'center', alignItems:'center', borderWidth:2, borderRadius:5,height:60,width:300, paddingHorizontal:10,borderStyle:'dashed', marginBottom:10, marginTop:20}}>   
                                <MaterialIcons
                                    name = "file-upload"
                                    size = {34}
                                />
                                <Text style = {{color:'blue', fontSize:10, width:"100%", textAlign:'center'}}>Upload multiple snapshots of your app.</Text>  
                            </View>
                        </TouchableOpacity>
                        {snapshots.length > 0 ?<View style = {{flexDirection:'row',justifyContent:'space-between'}}><Text style = {{marginTop:-9, fontSize:11,marginLeft:10,color:'green',marginBottom:10, textAlign:'right'}}>You have Uploaded {snapshots.length} images</Text><TouchableOpacity style = {{borderRadius:20, backgroundColor:'#D7E5DE',marginTop:-10}} onPress = {() =>{setModalVisible(true)}}><Text style = {{paddingTop:5,fontSize:11,color:'green', textAlign:'right', paddingHorizontal:20}}>View</Text></TouchableOpacity></View> : null}
                    </View>

                    {!isLoading && responseFromServer && (
                        <Text className = "font-bold" style = {{color:'green'}}>{responseFromServer}</Text>
                    )}
 
                    <LinearGradient
                        colors = {['rgba(9,181,211,0.9)','rgba(58,131,244,0.9)']}
                        end = {{x:1, y: 0.2}}
                        start={{x: 0.1, y:0.2}}
                        className = {`rounded-full flex justify-center items-center mt-5`}
                        style = {{width:'50%'}}
                    >
                        <TouchableOpacity
                            className = {`p-3 px-2`}
                            onPress = {handleUpload}
                        >
                            <Text className = "text-white font-bold">
                                {isLoading?<ActivityIndicator/>:"Upload"}
                            </Text>
                        </TouchableOpacity>

                    </LinearGradient>
                </View>
                <Modal
                    animationType = "slide"
                    transparent={true}
                    visible={snapshots.length > 0 ? modalVisible:false}
                    onRequestClose={() => {
                        Alert.alert("Modal has been closed.");
                    }}
                    blurRadius = {10}
                    overlayColor = "rgb(0,0,0,0.5)"
                >
                    <View style = {{justifyContent:'center', alignItems:'center',height:'100%'}}>
                        <View style={{justifyContent: 'center', alignItems: 'center', backgroundColor: 'white', width: 330, borderRadius:10, backgroundColor:'#D3D3D3', height:250 }}>
                            <FlatList
                                data = {snapshots}
                                renderItem = {({item}) =>(
                                    <View style = {{position:'relative'}}>
                                        <Image source = {{uri:item.uri}} style = {{width:100, height:100, marginTop:10, marginLeft:5}}/>
                                        <MaterialIcons
                                            name="clear"
                                            color="red"
                                            style = {{position:'absolute',top:"8%",left:'70%'}}
                                            size={30}
                                            onPress={() => {deleteImage(item.assetId)}}
                                        />
                                    </View>
                                )}
                                keyExtractor = {item => item.assetId}
                                numColumns = {3}
                            />
                            <TouchableOpacity
                                onPress={() => {
                                setModalVisible(!modalVisible);
                                }}
                                style = {{paddingHorizontal:10,paddingVertical:5, borderRadius:20, backgroundColor:"#D7E5DE", marginTop:10, marginBottom:10}}
                            >
                                <Text>Close</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Modal>
            </View>
        </LinearGradient>
    )
}

const styles = StyleSheet.create({
    container: {
        justifyContent:'center',
        alignItems:'center',
        marginTop:90
    },
    inputField: {
        borderRadius:10,
        marginBottom:10,
        paddingLeft:30
    },
    button: {
        backgroundColor: "#D7E5DE",
        borderRadius:20,
        paddingHorizontal:20,
        marginTop:20
    }
})