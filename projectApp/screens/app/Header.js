import { View,Image,Text,Dimensions, TextInput, TouchableOpacity } from "react-native"
import { useNavigation } from "@react-navigation/native";

const windowWidth = Dimensions.get('window').width;

export default function Header() {

    const navigation = useNavigation();

    return(
        <View style = {{flexDirection:'row', justifyContent:'space-between', alignItems:'center', width:windowWidth}}>
            <TouchableOpacity style = {{marginLeft:10}}
                onPress = {() => navigation.openDrawer()}
            >
                <Image
                    source={require("../../assets/logo/applogo.png")}
                    style = {{width:40,height:40}}
                />
            </TouchableOpacity>
            <TextInput
                placeholder="Search for apps...."
                style = {{backgroundColor: "#D7E5DE", borderRadius:20, width:windowWidth/1.5, paddingLeft:20, height:35}}
            />
            <TouchableOpacity
                style = {{borderRadius:50,width:40,height:40, marginRight:10,justifyContent:'center'}}
                onPress={() => {
                    navigation.openDrawer();
                }}
            >
                <Image
                    source={{uri:'https://www.communardo.com/wp-content/uploads/2017/01/User-Profiles_701x701.png'}}
                    style = {{width:40, height:40, borderRadius:50}}
                />
            </TouchableOpacity>
        </View>
    )
}