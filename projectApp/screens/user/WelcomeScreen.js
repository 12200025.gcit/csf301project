import React, { useEffect } from 'react'
import { View, Text, Image } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useNavigation } from '@react-navigation/native';

export default function WelcomeScreen() {

    const navigation = useNavigation();

    const getToken = async () => {
        try {
           const value = await AsyncStorage.getItem("token")
            if (value !== null) {
                navigation.replace("TabHome")
            } else {
                navigation.replace("Login")
            }
        } catch (error) {
            console.error(error.message);
        }
    }

    useEffect(() => {
        setTimeout(() => {
            getToken();
        },5000)
    },[])

  return (
    <LinearGradient
            colors = {['rgba(58,131,244,0.4)','rgba(9,181,211,0.4)']}
            className = 'w-full flex-1'
        >
            <View className = "flex-1 d-flex justify-center items-center">
                <Image
                    source={require("../../assets/logo/applogo.png")}
                    style = {{width:100,height:100}}
                />
            </View>
        </LinearGradient>
  )
}
