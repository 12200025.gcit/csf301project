import { View, Image, Text, TextInput, StyleSheet, TouchableOpacity } from "react-native";

export default function ResetPassword() {
    return(
        <View style = {{flex:1, justifyContent:'center', alignItems:'center'}}>
            <View>
                <Image
                    source={require('../../assets/logo/applogo.png')}
                    style = {{width:100,height:100}}
                />
            </View>
            <View >
                <Text>Reset Password</Text>
            </View>

            <View style = {{marginVertical:10}}>
                <TextInput placeholder="New Password"
                    style = {{...styles.inputField,borderWidth:1,width:300,height:40}}
                />
                <TextInput placeholder="Confirm Password"
                    style = {{...styles.inputField,borderWidth:1,width:300,height:40}}
                />
            </View>

            <TouchableOpacity 
                 style = {styles.button}
            >
                <Text style = {{paddingHorizontal:20, paddingVertical:10}}>Reset</Text>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    inputField: {
        borderRadius:15,
        marginBottom:10,
        paddingLeft:30
    },
    button: {
        backgroundColor: "#D7E5DE",
        borderWidth:1,
        borderRadius:20
    }
})