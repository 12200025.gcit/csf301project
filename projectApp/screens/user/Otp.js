import { View, Image, Text, TextInput, StyleSheet, TouchableOpacity } from "react-native";

export default function Otp({navigation,route}) {
    const {email} = route.params;

    return(
        <View style = {{flex:1, justifyContent:'center', alignItems:'center'}}>
            <View style = {{marginBottom:15}}>
                <Image
                    source={require('../../assets/logo/applogo.png')}
                    style = {{width:100,height:100}}
                />
            </View>
            <View style = {{marginBottom:15}}>
                <Text style = {{fontWeight:'bold'}}>It may take some time!</Text>
            </View>

            <View style = {{marginBottom:15}}>
                <Text style = {{fontWeight:'bold'}}>Enter your OTP</Text>
            </View>

            <View style = {{flexDirection:'row',justifyContent:'center',marginVertical:10, alignItems:'center', marginBottom:15}}>
                <TextInput
                    style = {{...styles.inputField,borderWidth:1, width:30,marginLeft:10}}
                />
                <TextInput
                    style = {{...styles.inputField,borderWidth:1, width:30,marginLeft:10}}
                />
                <TextInput
                    style = {{...styles.inputField,borderWidth:1, width:30,marginLeft:10}}
                />
                <TextInput
                    style = {{...styles.inputField,borderWidth:1, width:30,marginLeft:10}}
                />
            </View>

            <TouchableOpacity 
                 style = {styles.button}
            >
                <Text style = {{paddingHorizontal:20, paddingVertical:10}}>Reset</Text>
            </TouchableOpacity>
            <View style = {{marginBottom:15, marginTop: 15}}>
                <Text style = {{fontWeight:'bold'}}>You didn't get an email? RESEND</Text>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    inputField: {
        marginBottom:10,
    },
    button: {
        backgroundColor: "#D7E5DE",
        borderRadius:20
    }
})