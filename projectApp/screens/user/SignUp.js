import { TouchableOpacity, ActivityIndicator } from "react-native";
import { TextInput, StyleSheet } from "react-native";
import { View, Image, Text } from "react-native";
import { useEffect, useState } from "react";
import { auth, db } from "../../firebaseConfig";
import { createUserWithEmailAndPassword } from "firebase/auth";
import { doc, setDoc } from "firebase/firestore";

export default function SignUp({ navigation }) {
    const [email,setEmail] = useState();
    const [username,setUserName] = useState();
    const [password, setPassword] = useState();
    const [confirm,setConfirm] = useState();
    const [isFocused, setIsFocused] = useState(false);
    const [passwordError, setPasswordError] = useState("");
    const [emailError,setEmailError] = useState('');
    const [passwordLenghtError,setPasswordLengthError] = useState("");
    const [passwordFocused,setPasswordFocused] = useState(false);
    const [emailIsValid,setEmailIsValid] = useState(false);
    const [signUpError,setSignUpError] = useState("");
    const [isSigningUp,setIsSigningUP] = useState(false);


    useEffect(() =>  {
        if (confirm !== password) {
            setPasswordError("Password does not match")
        } else {
            setPasswordError('');
        }
    },[isFocused])

    async function storeToDatabase(id,mail) {
        const data = {
            email:mail,
            username:username,
            profilePath:''
        }

        if (emailIsValid && username !== '' && password === confirm) {
            await setDoc(doc(db,'users',id),data)
            .then(() => {
                // console.log("success")
            })
            .catch((error) => {
                console.error(error)
            })
        } else {
            setSignUpError("The field should not be empty.")
        }
    }

    function handleSignUp() {
        setIsSigningUP(true)
        createUserWithEmailAndPassword(auth,email,password)
        .then((userCredential) => {
            const user = userCredential.user;
            storeToDatabase(user.uid,user.email);
            setEmail("");
            setConfirm("");
            setPassword("");
            setUserName("");
            setIsSigningUP(false)
            navigation.navigate("Login")
        })
        .catch((error) => {
            const errorCode = error.code;
            const errorMessage = error.message;
            setSignUpError(errorMessage)
            setIsSigningUP(false)
        })
    }

    const validateEmail = () => {
        const emailPattern = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;
        if (!emailPattern.test(email)) {
            setEmailError("Please enter a valid email address");
            setEmailIsValid(false);
        } else {
            setEmailError('');
            setEmailIsValid(true);
        }
    }

    const handleEmailChange = (event) => {
        setEmail(event);
        validateEmail();
    }

    const handleUserNameChange = (event) => {
        setUserName(event);
    }

    const handlePasswordChange = (event) => {
        setPassword(event);
        if (event.length < 8) {
            setPasswordLengthError("Must is greater than 8 character")
        } else {
            setPasswordLengthError("");
        }
    }

    const handleConfirmChange = (event) => {
        setConfirm(event);
    }

    return(
        <View style = {{flex:1, justifyContent:'center', alignItems:'center', paddingBottom:isFocused?100:0}}>
            <View style = {{marginBottom:15}}>
                <Image
                    source={require('../../assets/logo/applogo.png')}
                    style = {{width:100,height:100}}
                />
            </View>
            <View style = {{marginBottom:10}}>
                <Text style = {{fontWeight:'bold'}}>Create An Account</Text>
            </View>
            <View style = {{marginVertical:10}}>
            {signUpError ? <View style = {{marginBottom:10, borderRadius:10, backgroundColor:'#FA8072'}}><Text style = {{padding:10,textAlign:'center', color:'white',paddingVertical:15,fontWeight:'bold'}}>{signUpError.split("/")[1].split(")")[0].split("-").join(" ")}</Text></View>:null}
                <TextInput 
                    value={email}
                    placeholder="Email"
                    onBlur={validateEmail}
                    style = {{...styles.inputField,borderWidth:1,width:300,height:40, borderColor:emailError?"red":'black'}}
                    onChangeText={handleEmailChange}
                />
                {emailError?<Text style = {{marginTop:-10, fontSize:10,marginLeft:10,color:'red', marginBottom:10}}>{emailError}</Text> : null}
                <TextInput 
                    value={username}
                    placeholder="Username"
                    style = {{...styles.inputField,borderWidth:1,width:300,height:40}}
                    onChangeText={handleUserNameChange}
                />
                <TextInput 
                    value={password}
                    secureTextEntry = {true}
                    placeholder="Password"
                    onFocus={() => {
                        setPasswordFocused(true);
                    }}
                    onBlur={() => {
                        setPasswordFocused(false);
                    }}
                    style = {{...styles.inputField,borderWidth:1,width:300,height:40,borderColor:passwordLenghtError && passwordFocused?"red":"black"}}
                    onChangeText={handlePasswordChange}
                />
                {passwordLenghtError && passwordFocused ?<Text style = {{marginTop:-10, fontSize:10,marginLeft:10,color:'red',marginBottom:10}}>{passwordLenghtError}</Text> : null}
                <TextInput 
                    value={confirm}
                    secureTextEntry = {true}
                    placeholder="Confirm Password"
                    onFocus={() => setIsFocused(true)}
                    onBlur={() => setIsFocused(false)}
                    style = {{...styles.inputField,borderWidth:1,width:300,height:40, borderColor:confirm !== password?"red":null}}
                    onChangeText={handleConfirmChange}
                />
                {passwordError && confirm !== password ?<Text style = {{marginTop:-10, fontSize:10,marginLeft:10,color:'red',marginBottom:10}}>{passwordError}</Text> : null}
            </View>
            <TouchableOpacity 
                 style = {styles.button}
                 onPress={handleSignUp}
            >
                <Text style = {{padding:10, paddingHorizontal:20}}>{isSigningUp?<ActivityIndicator/>:"Create Account"}</Text>
            </TouchableOpacity>

            <TouchableOpacity
                onPress={() => {
                    navigation.navigate("Login");
                }}
                style = {{marginTop:15}}
            >
                <Text style = {{fontWeight:'bold'}}>Already have an account?</Text>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    inputField: {
        borderRadius:15,
        marginBottom:10,
        paddingLeft:30
    },
    button: {
        backgroundColor: "#D7E5DE",
        borderRadius:20,
        marginTop:10
    }
})