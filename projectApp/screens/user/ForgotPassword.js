import { useEffect, useState } from "react";
import { View, Image, Text, TextInput, StyleSheet, TouchableOpacity } from "react-native";
import { verifyEmail } from "../Server";

export default function ForgotPassword({navigation}) {

    const [email,setEmail] = useState("");
    const [sendingLink,setSendingLink] = useState();
    const [emailError,setEmailError] = useState();

    const changePassword = async () => {
        if (!email) {
            setEmailError("Enter your email")
            setSendingLink("");
            return;
        }
        try {
            setSendingLink("You may receive your mail in 5 seconds")
            setEmailError('');
            await verifyEmail(email);
            setTimeout(() => {
                navigation.navigate("Login")
            },5000)
        } catch (error) {
            console.error(error.message);
        }
    }

    return(
        <View style = {{flex:1, justifyContent:'center', alignItems:'center'}}>
            <View style = {{marginBottom:15}}>
                <Image
                    source={require('../../assets/logo/applogo.png')}
                    style = {{width:100,height:100}}
                />
            </View>
            <View style = {{marginBottom:15}}>
                <Text style = {{fontWeight:'bold'}}>Forgot Your Password?</Text>
            </View>

            <View style = {{marginBottom:15}}>
                <Text>Reset code would be sent through mail</Text>
            </View>

            {emailError ? <View style = {{width:'80%',marginBottom:10, borderRadius:10, backgroundColor:'#FA8072'}}><Text style = {{padding:10,textAlign:'center', color:'white',paddingVertical:15,fontWeight:'bold'}}>{emailError}</Text></View>:null}

            {sendingLink ? <View style = {{width:'80%',marginBottom:10, borderRadius:10, backgroundColor:'green'}}><Text style = {{padding:10,textAlign:'center', color:'white',paddingVertical:15,fontWeight:'bold'}}>{sendingLink}</Text></View>:null}
            <View style = {{marginVertical:10,marginBottom:20}}>
                <TextInput placeholder="Email"
                    value = {email}
                    onChangeText = {(event) => setEmail(event)}
                    style = {{...styles.inputField,borderWidth:1,width:300,height:40}}
                />
            </View>

            <TouchableOpacity 
                 style = {styles.button}
                 onPress={() => {
                    changePassword();
                 }}
            >
                <Text style = {{paddingHorizontal:30, paddingVertical:10}}>Send</Text>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    inputField: {
        borderRadius:15,
        marginBottom:10,
        paddingLeft:30
    },
    button: {
        backgroundColor: "#D7E5DE",
        borderRadius:20
    }
})