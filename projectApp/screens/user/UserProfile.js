import { View, Text, Image, StyleSheet, Dimensions, TextInput, TouchableOpacity, Alert } from "react-native";
import ProfileHeader from "./ProfileHeader";
import { LinearGradient } from "expo-linear-gradient";
import { useEffect, useState } from "react";
import { onAuthStateChanged } from "firebase/auth";
import { auth } from "../../firebaseConfig";
import { getUser,updateEmailUser,verifyEmail, getSingleUserApp, deleteApp } from "../Server";
import { ScrollView } from "react-native-gesture-handler";
import { AntDesign } from "@expo/vector-icons";
import AsyncStorage from "@react-native-async-storage/async-storage";

export default function UserProfile({navigation}) {

    const windowWidth = Dimensions.get("window").width;

    const [userId,setUserId] = useState();
    const [user,setUser] = useState([]);
    const [email,setEmail] = useState("");
    const [userName,setUserName] = useState("");
    const [userUpdatePrompt,setUserUpdatePrompt] = useState();
    const [emailError,setEmailError] = useState();
    const [emailIsValid,setEmailIsValid] = useState(true);
    const [response,setResponse] = useState();
    const [sendingLink,setSendingLink] = useState();
    const [userApp,setUserApp] = useState([]);


    useEffect(() => {
        const gettingToken = async () => {
            const userId = await AsyncStorage.getItem("token")
            if (userId !== null) {
                setUserId(userId)
            }
        }
        gettingToken();
    },[])

    const deletingApp = async (app_id,snapshots,filePath) => {
        console.log("sending app id ",app_id)
        await deleteApp(app_id,snapshots,filePath);
    }

    const getCurrentUser = async () => {
        try {
            setUser(await getUser(userId));
        } catch (error) {
            console.log(error.message)
        }
    }

    const validateEmail = () => {
        const emailPattern = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;
        if (!emailPattern.test(email)) {
            setEmailError("Please enter a valid email address");
            setEmailIsValid(false);
        } else {
            setEmailError('');
            setEmailIsValid(true);
        }
    }

    const changeUserDetails = async () => {
        try {
            if (user.email === email && user.username === userName) {
                setUserUpdatePrompt("Nothing to update!")
                return;
            } else if (user.email !== email || user.username !== userName) {
                validateEmail(email)
                if (emailIsValid) {
                    console.log("Email is validated")
                    setResponse(await updateEmailUser(email,userName));
                    userUpdatePrompt("Successfully Updated!")
                }
            }
        } catch (error) {
            console.log(error.message)
        }
    }

    const changeUserPassword = async () => {
        setSendingLink("Please check your mail")
        try {
            verifyEmail(email);
        } catch (error) {
            console.log(error.message);
        }
    }

    const getAllApp = async () => {
        try {
            setUserApp(await getSingleUserApp(userId));
        } catch (error) {
            console.log(error.message);
        }
    }

    const handleLongPress = (app_id,snapshots,filePath) => {
        console.log("sending app id from handle long press ",app_id)
        console.log("from user profile ",snapshots)
        console.log(" ", filePath)
        try {
            Alert.alert(
                "Delete App",
                "Are you sure you want to delete?",
                [
                    {
                        text:'Cancel',
                        style:"cancel"
                    },
                    {
                        text:"Delete",
                        onPress: () => deletingApp(app_id,snapshots,filePath),
                        style: "destructive"
                    }
                ]
            )
        } catch (error) {
            console.log(error.message)
        }
    }

    useEffect(() => {
        getCurrentUser();
        getAllApp();
        return;
    },[userId])

    useEffect(() => {
        if (user) {
            setEmail(user.email)
            setUserName(user.username)
        }
    },[user])
    
    useEffect(() => {
        let unsubscribe;
        const fetchData = async () => {
            unsubscribe = await getSingleUserApp(userId,async (updatedData) => {
                console.log("Updated data ", updatedData)
                setUserApp(updatedData);
            })
        }
        fetchData();
        return () => {
            if (unsubscribe && typeof unsubscribe === 'function') {
                console.log("Calling again")
                unsubscribe();
            }
        };
    },[userId])

    console.log(userId)
    console.log("user ", user)
    console.log("email error ", emailError);
    console.log("email validated ", emailIsValid);
    console.log("from client ", userApp)

    return(
        <LinearGradient
            colors = {['rgba(58,131,244,0.4)','rgba(9,181,211,0.4)']}
            className = 'w-full flex-1'
        >
        <ScrollView>
            <ProfileHeader/>
            <View>
                <View className = "ml-5 mt-1">
                    <Text className = "font-bold" style = {{fontSize:20}}>User Details</Text>
                </View>
            </View>
            <View className = "mx-10" style = {{marginTop:10, borderRadius:10, backgroundColor:"#D7E5DE"}}>
                {user && 
                    <View
                        className = 'w-full flex-1 flex-row justify-center items-center'
                        style = {{borderRadius:10}}
                    >
                        <View className = "my-4 text-center px-5" style = {{width:'100%'}}>
                            <TextInput className = "px-3 font-bold" value = {userName}
                                style = {{borderWidth:1,height:50,borderRadius:10}}
                                onChangeText = {(event) => setUserName(event)}
                            />
                            <TextInput className = "mt-4 px-3 font-bold" value = {email}
                                style = {{borderWidth:1,height:50,borderRadius:10}}
                                onChangeText = {(event) => setEmail(event)}
                            />

                            {userUpdatePrompt &&
                                <View className = "justify-center items-center mt-4">
                                    <Text style = {{color:"#FE4C00"}}>{userUpdatePrompt}</Text>
                                </View>
                            }

                            {response && 
                                <View className = "justify-center items-center mt-4">
                                    <Text style = {{color:"#FE4C00"}}>{response}</Text>
                                </View>
                            }
                            
                            <LinearGradient
                                colors = {['rgba(9,181,211,0.9)','rgba(58,131,244,0.9)']}
                                end = {{x:1, y: 0.2}}
                                start={{x: 0.1, y:0.2}}
                                className = {`rounded-full items-center px-5 mt-4`}
                            >
                                <TouchableOpacity
                                    className = {`p-3 px-4`}
                                    onPress = {changeUserDetails}
                                >
                                    <Text className = "text-white font-bold">
                                        Update
                                    </Text>
                                </TouchableOpacity>

                            </LinearGradient>

                            <View>
                                <View className = "mt-4">
                                    {sendingLink && 
                                        <Text className = "font-bold text-center" style = {{color:'green',marginBottom:-10}}>{sendingLink}</Text>
                                    }
                                </View>

                                <LinearGradient
                                    colors = {['rgba(9,181,211,0.9)','rgba(58,131,244,0.9)']}
                                    end = {{x:1, y: 0.2}}
                                    start={{x: 0.1, y:0.2}}
                                    className = {`rounded-full items-center px-5 mt-4`}
                                >
                                    <TouchableOpacity
                                        className = {`p-3 px-4`}
                                        onPress={changeUserPassword}
                                    >
                                        <Text className = "text-white font-bold">
                                            Change Password
                                        </Text>
                                    </TouchableOpacity>

                                </LinearGradient>
                            </View>
                            

                        </View>
                    </View> 
                }
            </View>
            <View>
                <View className = "ml-5 mt-5">
                    <Text className = "font-bold" style = {{fontSize:20}}>Your App</Text>
                </View>

                <ScrollView className = "mt-4" style = {{height:500}} showsVerticalScrollIndicator = {false}>
                        {userApp && userApp.length !== 0 ?
                            (
                                userApp.map((item,index) => {
                                    return (
                                        <TouchableOpacity
                                            key={index}
                                            className = "mx-4 p-2 mb-2 flex-row border"
                                            style = {{borderRadius:10}}
                                            onPress={() => {
                                                navigation.navigate("Preview",{
                                                    image:item.imageUrl,
                                                    name:item.name,
                                                    snapshots:item.snapshotsImageUrl,
                                                    user_id:userId,
                                                    app_id:item.doc_id,
                                                    category:item.category,
                                                    description:item.description,
                                                    features:item.features,
                                                    whatsNew:item.whatsNew
                                                })
                                            }}
                                            onLongPress={() => handleLongPress(item.doc_id,item.snapshots,item.filePath)}
                                        >
                                            <Image source = {{uri: item.imageUrl}} style = {{width:60,height:60}} className = "rounded-2xl"/>
                                            <View className = "flex-1 flex justify-center pl-3 space-y-3">
                                                <Text className = "font-semibold">{item.name}</Text>
                                                <View className = "flex-row items-center">
                                                    <AntDesign
                                                        name = "download"
                                                    />
                                                    <Text className = "text-gray-500 ml-1">{item.download}</Text>
                                                </View>
                                            </View>
                                            <View className = "flex justify-center">
                                            <LinearGradient
                                                colors = {['rgba(9,181,211,0.9)','rgba(58,131,244,0.9)']}
                                                end = {{x:1, y: 0.2}}
                                                start={{x: 0.1, y:0.2}}
                                                className = {`rounded-full items-center px-3`}
                                            >
                                                <TouchableOpacity
                                                    className = {`p-2 px-1`}
                                                    onPress={() => navigation.navigate("description",{
                                                        appToEdit:item
                                                    })}
                                                >
                                                    <Text className = "text-white font-bold">
                                                        Description
                                                    </Text>
                                                </TouchableOpacity>
    
                                            </LinearGradient>
                                            </View>
                                        </TouchableOpacity>
                                    )
                                })
                            ):(
                                <View className="mt-5">
                                    <Text className="text-center" style={{ fontSize: 40 }}>
                                        🫥
                                    </Text>
                                    <Text className="text-center font-bold" style={{ fontSize: 10 }}>
                                        Currently you don't have any application
                                    </Text>
                                </View>
                            )
                        }
                    </ScrollView>
            </View>
        </ScrollView>
        </LinearGradient>
    )
}

const styles = StyleSheet.create({
    absolute: {
        position:'absolute',
        top:-100,
        right:100,
    }
})