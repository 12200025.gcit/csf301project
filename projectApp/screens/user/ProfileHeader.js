import { View } from "react-native"
import { FontAwesome } from "@expo/vector-icons"
import { useNavigation } from "@react-navigation/native"
import { auth } from "../../firebaseConfig";
import { signOut } from "firebase/auth";
import AsyncStorage from '@react-native-async-storage/async-storage';

export default function PreviewHeader() {

    const navigation = useNavigation();

    function handleSignOut() {
        signOut(auth)
        .then(async () => {
            await AsyncStorage.removeItem("token")
            navigation.replace("Login")
        })
        .catch((error) => {
            console.error(error);
        })
    }

    return(
        <View style = {{marginTop:40, flexDirection:'row', justifyContent:'flex-end', alignItems:'center'}}>

            <FontAwesome
                name="sign-out"
                size={32}
                style = {{marginRight:20,color:"#FE4C00"}}
                onPress={handleSignOut}
            />
        </View>
    )
}