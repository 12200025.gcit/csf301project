import { useState } from "react";
import { View, Image, Text, TextInput, StyleSheet, TouchableOpacity, ActivityIndicator } from "react-native";
import { auth } from "../../firebaseConfig";
import { signInWithEmailAndPassword } from "firebase/auth";
import AsyncStorage from '@react-native-async-storage/async-storage';

export default function Login({ navigation }) {

    const [email,setEmail] = useState();
    const [password, setPassword] = useState();
    const [loginError,setLoginError] = useState("");
    const [loggingin,setLoggingIn] = useState(false)

    function handleEmailChange(event) {
        setEmail(event);
    }

    function handlePasswordChange(event) {
        setPassword(event);
    }

    function handleLogin() {
        setLoggingIn(true)
        signInWithEmailAndPassword(auth,email,password)
        .then(async (userCredential) => {
            const user = userCredential.user;
            if (user.email === "12200035.gcit@rub.edu.bt") {
                navigation.navigate("admintab");
                setEmail("")
                setPassword("");
            } else {
                await AsyncStorage.setItem("token",user.uid);
                navigation.replace("TabHome");
                setEmail("")
                setPassword("");
            }
            setLoggingIn(false)
            loginError("");
        })
        .catch((error) => {
            setLoginError(error.message.split(":")[1]);
            setLoggingIn(false)
        })
    }

    return(
        <View style = {{flex:1, justifyContent:'center', alignItems:'center'}}>
            <View style = {{marginBottom:20}}>
                <Image
                    source={require('../../assets/logo/applogo.png')}
                    style = {{width:100,height:100}}
                />
            </View>
            <View style = {{marginBottom:10}}>
                <Text style = {{fontWeight:'bold'}}>Welcome Back 👏👏👏</Text>
            </View>

            <View style = {{marginVertical:10}}>
                {loginError ? <View style = {{marginBottom:10, borderRadius:10, backgroundColor:'#FA8072'}}><Text style = {{padding:10,textAlign:'center', color:'white',paddingVertical:15,fontWeight:'bold'}}>{loginError.split("/")[1].split(")")[0].split("-").join(" ")}</Text></View>:null}
                <TextInput placeholder="Email"
                    value={email}
                    style = {{...styles.inputField,...styles.borderShadow,borderWidth:1,width:300,height:40}}
                    onChangeText={handleEmailChange}
                />
                <TextInput secureTextEntry = {true} placeholder="Password"
                    value={password}
                    style = {{...styles.inputField,borderWidth:1,width:300,height:40}}
                    onChangeText={handlePasswordChange}
                />
            </View>
            <TouchableOpacity 
                style = {{marginTop:-15,marginLeft:'50%',marginBottom:15}}
                onPress={() => {
                    navigation.navigate("ForgotPassword");
                }}
            >
                <Text style = {{fontSize:10, color:'blue', fontWeight:'bold'}}>Forgot your password?</Text>
            </TouchableOpacity>
            
            <TouchableOpacity 
                onPress={handleLogin}
                 style = {styles.button}
            >
                <Text style = {{paddingHorizontal:30, paddingVertical:10}}>{loggingin?<ActivityIndicator/>:"Login"}</Text>
            </TouchableOpacity>

            <TouchableOpacity
                onPress={() => {
                    navigation.navigate("SignUp")
                }}
            >
                <Text style = {{fontWeight:'bold'}}>Don't have an account?</Text>
            </TouchableOpacity>
            
        </View>
    )
}

const styles = StyleSheet.create({
    inputField: {
        borderRadius:15,
        marginBottom:10,
        paddingLeft:30,
    },
    button: {
        backgroundColor: "#D7E5DE",
        borderRadius:20,
        marginBottom:15
    },
    borderShadow: {
        shadowColor:'black',
        elevation: 0,
    }
})