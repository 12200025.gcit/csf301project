// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import {getAuth} from 'firebase/auth'; 
import {getFirestore} from 'firebase/firestore';
import {getStorage} from 'firebase/storage';

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyACRrKaRI2cXGR655HVs8GsI7MDmPBsnSw",
  authDomain: "appcrate-7e3cf.firebaseapp.com",
  projectId: "appcrate-7e3cf",
  storageBucket: "appcrate-7e3cf.appspot.com",
  messagingSenderId: "297851278540",
  appId: "1:297851278540:web:3d7bbcbe59f189cf65de69"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const auth = getAuth(app);
export const db = getFirestore(app);
export const storage = getStorage(app);
