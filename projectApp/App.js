import { StatusBar } from 'expo-status-bar';
import { StyleSheet} from 'react-native';
import SignUp from './screens/user/SignUp';
import Login from './screens/user/Login';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Preview from './screens/app/Preview';
import UserProfile from './screens/user/UserProfile';
import ForgotPassword from './screens/user/ForgotPassword';
import Otp from './screens/user/Otp';
import { TailwindProvider } from 'tailwindcss-react-native';
import MyDrawer from './screens/app/tabs/MyDrawer';
import AboutApp from './screens/app/tabs/AboutApp';
import Description from './screens/app/tabs/Description';
import Admin from './screens/app/admin/Admin';
import BottomTab from './screens/app/tabs/BottomTab';
import WelcomeScreen from './screens/user/WelcomeScreen';

const stack = createNativeStackNavigator();

export default function App() {
  return (
    <TailwindProvider>
      <NavigationContainer>
        <stack.Navigator
          screenOptions={{
            headerShown: false
          }}
        >
          <stack.Screen name = "Welcome" component = {WelcomeScreen}/>
          <stack.Screen name = "Login" component={Login}/>
          <stack.Screen name = "SignUp" component={SignUp}/>
          <stack.Screen name = "TabHome" component={MyDrawer}/>
          <stack.Screen name = "Preview" component={Preview}/>
          <stack.Screen name = "Profile" component={UserProfile}/>
          <stack.Screen name = "ForgotPassword" component={ForgotPassword}/>
          <stack.Screen name = "OTP" component={Otp}/>
          <stack.Screen name = "about" component={AboutApp}/>
          <stack.Screen name = "description" component={Description}/>
          <stack.Screen name = "admin" component={Admin}/>
          <stack.Screen name = "admintab" component={BottomTab}/>
        </stack.Navigator>
      </NavigationContainer>
    </TailwindProvider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    marginTop:40
  },
});
